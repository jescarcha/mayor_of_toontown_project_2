<?
    session_start();

    require("db_connect.php");

    mysql_select_db("ja033522", $mysqli);
    
    if(isset($_SESSION['voted'])) {
        unset($_SESSION['voted']);
    }
    
    if(isset($_SESSION['voter_id'])) {
        unset($_SESSION['voter_id']);
    }
    
    if(isset($_POST['login']) && (!empty($_POST['voter_id'])) && (!preg_match("/^(1000)$/", $_POST['voter_id'])) && (preg_match("/^(10)([0-4][0-9])$/", $_POST['voter_id']))) {
        
        $_SESSION['voter_id'] = $_POST['voter_id'];

        $result = mysql_query("SELECT * FROM p2_votes");
        if($mysqli->error) {
            print "Select query error!  Message: ".$mysqli->error;
        }
        
        while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
            if ($_SESSION['voter_id'] == $row['voter_id']) { 
                $_SESSION['voted'] = true;
                header('location: results.php');
                break 2;
            }
        }
        
        $insert_query = "INSERT INTO p2_votes (voter_id)
            VALUES (
                '".$_SESSION['voter_id']."'
            )";
        $insert_result = mysql_query($insert_query);
        if($mysqli->error) {
            print "Insert query failed: ".$mysqli->error;
        }
        header('location: candidates.php');
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0053)http://www.cs.ucf.edu/~jmmoshell/DIG4104c/books4.html -->
<html xmlns="http://www.w3.org/1999/xhtml" class="ui-mobile">
	<head>
		<?php include('includes/head.php') ?>
	</head> 

	<body class="ui-mobile-viewport ui-overlay-c"> 
		
		<center>

		<!-- Start of first page -->
		<div data-role="page" id="page1" data-url="page1" tabindex="0" class="ui-page ui-body-c ui-page-active" style="min-height: 538px; ">

		<div data-role="header" class="ui-header ui-bar-a" role="banner">
			<h1 class="ui-title" role="heading" aria-level="1">Mayor of Toontown Elections</h1>
		</div><!-- /header -->

            <img src="p2_files/toon_town_logo.png" alt="toontown logo" style="width: 80%; max-width: 484px; margin-top:20px;"/>

			<center><div id="logo"><h1>Log in to vote now!</h1></div></center>

			<div data-role="content" class="ui-content" role="main">	
               
				<form action="" method="post" data-ajax="false">
		    		<input type="text" name="voter_id" id="voter_id" placeholder="Enter your voter ID" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset">
		    		<? if (isset($_POST['login']) &&(!empty($_POST['voter_id'])) && (!preg_match("/^(10)([0-4][0-9])$/", $_POST['voter_id']))) {
                    ?>
                    <p>Please enter a valid Voter ID!</p>
                    <?
                    }
                    ?>
					<br />

					<input id="login" name="login" type="submit" value="Login" data-role="button" data-theme="a" /><br />
		    		<div>
						<a href="public_results.php" data-role="button" data-theme="a" rel="external">View Current Results</a>
			     	</div> 
				</form>
			</div><!-- /content -->

			<div data-role="footer" class="ui-footer ui-bar-a" role="contentinfo">
				<h4 class="ui-title" role="heading" aria-level="1"></h4>
			</div><!-- /footer -->
		</div><!-- /page -->
	</body>
</html>

<?
    $mysqli->close();
?>