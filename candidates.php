<?
    session_start();
    
    require("db_connect.php");

    mysql_select_db("ja033522", $mysqli);
    
	if (!$_SESSION['voter_id']) {
		header('location: p2.php');
	}
	
    $vote_to_update = $_SESSION['voter_id'];
    
    if(isset($_POST['bugs_vote'])) {
        
        $update_query = "UPDATE p2_votes SET bugs_vote = 'vote' WHERE voter_id = '$vote_to_update'";
        $update_vote = mysql_query($update_query);
        if($mysqli->error) {
            print "Insert query failed: ".$mysqli->error;
        }
        $_SESSION['voted'] = true;
        header('location: results.php');
    }
    
    if(isset($_POST['mickey_vote'])) {
        
        $update_query = "UPDATE p2_votes SET mickey_vote = 'vote' WHERE voter_id = '$vote_to_update'";
        $update_vote = mysql_query($update_query);
        if($mysqli->error) {
            print "Insert query failed: ".$mysqli->error;
        }
        $_SESSION['voted'] = true;
        header('location: results.php');
    }
    
    if(isset($_POST['wile_vote'])) {
        
        $update_query = "UPDATE p2_votes SET wiley_vote = 'vote' WHERE voter_id = '$vote_to_update'";
        $update_vote = mysql_query($update_query);
        if($mysqli->error) {
            print "Insert query failed: ".$mysqli->error;
        }
        $_SESSION['voted'] = true;
        header('location: results.php');
    }
    
    if ($_SESSION['voted']) {
        $bv_num = 0;
        $mv_num = 0;
        $wv_num = 0;
        
        $result = $update_vote = mysql_query("SELECT * FROM p2_votes");
        
        if($mysqli->error) {
            print "Select query error!  Message: ".$mysqli->error;
        }
        
        while($row = mysql_fetch_assoc($result)) {
            if ($row['bugs_vote'] === 'vote') { 
                ++$bv_num;
            }
            if ($row['mickey_vote'] === 'vote') { 
                ++$mv_num;
            }
            if ($row['wiley_vote'] === 'vote') { 
                ++$wv_num;
            }
        }
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0053)http://www.cs.ucf.edu/~jmmoshell/DIG4104c/books4.html -->
<html xmlns="http://www.w3.org/1999/xhtml" class="ui-mobile">
	<head>
		<?php include('includes/head.php') ?>
	</head> 

	<body class="ui-mobile-viewport ui-overlay-c"> 
		
		<center>

		<!-- First Candidate Page -->
		<div data-role="page" id="candidate1" data-url="candidate1" data-theme="a">
			<div data-role="header">
				<a href="p2.php" data-role="button" data-theme="a" rel="external">Home</a>
				<h1>Bugs Bunny</h1>
				<a href="results.php" data-role="button" data-theme="a" rel="external">Results</a>
			</div><!-- /header -->
    
			<div data-role="content" data-theme="a">	
				<img src="p2_files/bugs.jpg" class="candidate_image" />
				<p>A witty and cunning candidate, Bugs Bunny wants to make Toontown a fun and entertaining place to live. With Bugs Bunny as our mayor, rabbit season will be abolished and it will forever be duck season.</p>
				<? if (!$_SESSION['voted']) {
                ?> 
                <form name="bugsVoteForm" method="post" action="" data-ajax="false">
                    <fieldset>
                       <input id="bugs_vote" name="bugs_vote" type="submit" value="Vote for Bugs!" /><br />
                    </fieldset>
                </form> 
                <?
                    } else {
                    ?>
                    <p><? print "Votes for Bugs Bunny: ".$bv_num; ?></p>
                    <?
                    }
                ?>
			</div><!-- /content -->	
    	         
			<div data-theme="a">
		    	<a href="candidates.php#candidate2" data-role="button" data-inline="true" style="float: right">Next</a>	
			</div>

			<br /><br /><br /><br />

			<div data-role="footer" data-theme="a">
				<h4> </h4>
			</div><!-- /footer -->
		</div><!-- /page -->

		<!-- Second Candidate Page -->
		<div data-role="page" id="candidate2" data-url="candidate2" data-theme="a">

			<div data-role="header">
				<a href="p2.php" data-role="button" data-theme="a" rel="external">Home</a>
				<h1>Mickey Mouse</h1>
				<a href="results.php" data-role="button" data-theme="a" rel="external">Results</a>
			</div><!-- /header -->
    
			<div data-role="content" data-theme="a">	
				<img src="p2_files/mickey.jpg" class="candidate_image"/>
				<p>Mickey Mouse is one of the oldest and wisest characters in Toontown. Without him, many of the toons wouldn't even be there today. Mickey Mouse is a humble and kind candidate. And a vote for him is a vote for toons everywhere!</p>
		    	<? if (!$_SESSION['voted']) {
                ?> 
                <form name="mickeyVoteForm" method="post" action="" data-ajax="false">
                    <fieldset>
                       <input id="mickey_vote" name="mickey_vote" type="submit" value="Vote for Mickey!" /><br />
                    </fieldset>
                </form> 
                <?
                    } else {
                    ?>
                    <p><? print "Votes for Mickey Mouse: ".$mv_num; ?></p>
                    <?
                    }
                ?>
			</div><!-- /content -->	
    	         
			<div>
				<a href="candidates.php#candidate1" data-role="button" data-inline="true" style="float: left">Previous</a>
		    	<a href="candidates.php#candidate3" data-role="button" data-inline="true" style="float: right">Next</a>	
			</div>

			<br /><br /><br /><br />

			<div data-role="footer">
				<h4> </h4>
			</div><!-- /footer -->
		</div><!-- /page -->

		<!-- Third Candidate Page -->
		<div data-role="page" id="candidate3" data-url="candidate3" data-theme="a">

			<div data-role="header">
				<a href="p2.php" data-role="button" data-theme="a" rel="external">Home</a>
				<h1>Wile E Coyote</h1>
				<a href="results.php" data-role="button" data-theme="a" rel="external">Results</a>
			</div><!-- /header -->
    
			<div data-role="content" data-theme="a">	
				<img src="p2_files/coyote.jpg" class="candidate_image" />
				<p>Wile E Coyote may be a very quiet candidate, but he is a great military leader and weapons expert. With Wile E Coyote as your mayor, you will never have to worry about another takeover of Toontown.</p>
		    	<? if (!$_SESSION['voted']) {
                ?> 
                <form name="wileyVoteForm" method="post" action="" data-ajax="false">
                    <fieldset>
                       <input id="wile_vote" name="wile_vote" type="submit" value="Vote for Wile E!" /><br />
                    </fieldset>
                </form> 
                <?
                    } else {
                    ?>
                    <p><? print "Votes for Wile E Coyote: ".$wv_num; ?></p>
                    <?
                    }
                ?>
			</div><!-- /content -->	
    	         
			<div>
		    	<a href="candidates.php#candidate2" data-role="button" data-inline="true" style="float: left">Previous</a>	
			</div>

			<br /><br /><br /><br />

			<div data-role="footer">
				<h4> </h4>
			</div><!-- /footer -->
		</div><!-- /page -->
	</body>
</html>

<?
    $mysqli->close();
?>