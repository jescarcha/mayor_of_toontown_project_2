<?
    session_start();
    
    require("db_connect.php");
    
    mysql_select_db("ja033522", $mysqli);

	if (!$_SESSION['voter_id']) {
		header('location: p2.php');
	}
    
	$bv_num = 0;
    $mv_num = 0;
    $wv_num = 0;
        
    $result_query = "SELECT * FROM p2_votes";    
    $result = mysql_query("SELECT * FROM p2_votes");
        
    if($mysqli->error) {
        print "Select query error!  Message: ".$mysqli->error;
    }
        
    while($row = mysql_fetch_assoc($result)) {
        if ($row['bugs_vote'] === 'vote') { 
            ++$bv_num;
        }
        if ($row['mickey_vote'] === 'vote') { 
            ++$mv_num;
        }
        if ($row['wiley_vote'] === 'vote') { 
            ++$wv_num;
        }
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0053)http://www.cs.ucf.edu/~jmmoshell/DIG4104c/books4.html -->
<html xmlns="http://www.w3.org/1999/xhtml" class="ui-mobile">
	<head>
		<?php include('includes/head.php') ?>
	</head> 

	<body class="ui-mobile-viewport ui-overlay-c"> 
		<!-- Results Page -->
		<div data-role="page" id="results" data-url="results" data-theme="a">

			<div data-role="header" data-theme="a">
				<h1>Results</h1>
			</div><!-- /header -->
    
			<div data-role="content" id="candidates_results" data-theme="a">	
				<div id="candidate1">
					<div class="results_left">
						<img src="p2_files/bugs.jpg" class="results_pic" />
					</div>
					<div class="results_right">
						<h2>Bugs Bunny</h2>
						<p><? print $bv_num." votes"; ?></p>
					</div>
				</div>
				
				<div class="clear"></div>
				
				<div id="candidate2">
					<div class="results_left">
						<img src="p2_files/mickey.jpg" class="results_pic" />
					</div>
					<div class="results_right">
						<h2>Mickey Mouse</h2>
						<p><? print $mv_num." votes"; ?></p>
					</div>
				</div>
				
				<div class="clear"></div>
				
				<div id="candidate3">
					<div class="results_left">
						<img src="p2_files/coyote.jpg" class="results_pic" />
					</div>
					<div class="results_right">
						<h2>Wile E Coyote</h2>
						<p><? print $wv_num." votes"; ?></p>
					</div>
				</div>
				
				<div class="clear"></div>

				<? if ($_SESSION['voted']) {
                    print "You have already voted! Thank you for submitting your vote!";
                ?>
                <a href="candidates.php" data-role="button" data-theme="a" rel="external">Return to Candidates</a>
                <?
                    }
                ?>
			</div><!-- /content -->	

			<div data-role="footer">
				<h4> </h4>
			</div><!-- /footer -->
		</div><!-- /page -->
		
	</body>
</html>