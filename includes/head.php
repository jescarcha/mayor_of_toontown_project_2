<?php /* Save yourself from a world of redundancy! */ ?>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href=".">
<title>Project 2</title> 
<meta name="viewport" content="width=device-width, initial-scale=1"> 

<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0-beta.1/jquery.mobile-1.2.0-beta.1.min.css">
<link rel="stylesheet" href="themes/patriotic.min.css" />
<link rel="stylesheet" href="themes/custom.css" />
<script src="./p2_files/jquery-1.7.2.min.js"></script>
<script src="./p2_files/jquery.mobile-1.2.0-beta.1.min.js"></script>

<?php /* #endredundancy */ ?>